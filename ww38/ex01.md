20A_ITT1_NETWORKING
Exercise 01

Resources for the OSI model
What is good googling? 

An article, Wikipedia, ENG
https://en.wikipedia.org/wiki/OSI_model

The site is very trustworthy, despite being Wikipedia. Considering the length of the article, it would be a bigger project. Therefore, only certified people can work on it. The article has nearly 40 references also. Therefore, it appears to be pretty authoritative. There are some minor differences in the graphs compared to MON’s lecture, but the documentation seems solid.

An article, Cloudflare, ENG
https://www.cloudflare.com/learning/ddos/glossary/open-systems-interconnection-model-osi/

This site, page one on Google, also seems very authoritative. It has a big support page, with “Trust and Safety”[1], links to their huge LinkedIn[2] page, and even their own user(login) system with purchasable internet security packages. 

A Video, Eli the Computer Guy
https://www.youtube.com/watch?v=HEEnLZV2wGI&ab_channel=ElitheComputerGuy

This video is made by Eli the Computer Guy[3], which is a channel with over a million subscribers, and his videos often surpass a million viewers. This author has his own webpage[4], covering all kinds of different subjects revolving around the same theme (technology), with project notes[5] and videos[6].


A Video, RealPars
https://www.youtube.com/watch?v=Ilk7UXzV_Qc&ab_channel=RealPars

This video, made by RealPars[7], also explains the OSI model. The channel is verified by YouTube and has over half a million subscribers. The video itself has more than 200.000 views and was posted last year. The description is long and describes the subject. They also offer an article[8] that further describes the OSI model, and it’s meaning, on their website[9] which in itself seems very authoritative. 

[1]https://www.cloudflare.com/abuse/?utm_referrer=https://www.google.com/

[2]https://www.linkedin.com/company/cloudflare 

[3]https://www.youtube.com/watch?v=HEEnLZV2wGI&ab_channel=ElitheComputerGuy

[4]https://www.elithecomputerguy.com/ 

[5]https://www.elithecomputerguy.com/project-notes/ 

[6]https://www.elithecomputerguy.com/videos/ 

[7]https://www.youtube.com/channel/UCUKKQwBQZczpYzETkZNxi-w 

[8]https://realpars.com/osi/ 

[9]https://realpars.com 
